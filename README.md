# PSTR
Power Toggle

## Ide
Kenapa? SLA dari server harus selalu dijaga tetap tinggi dan PSBB yang berlangsung pada
beberapa bulan terakhir berdampak pada server komunitas di Himalkom tidak dapat dihidupkan
secara otomatis ketika pasca terjadi pemadaman listrik. Solusi yang ditawarkan cukup sederhana
dan tidak ada wiring langsung ke power dari komputer untuk mencegah faktor-faktor yang
tidak terduga karena perangkat bisa rusak jika ada kesalahan pada rangkaian. Servo sebagai
aktuator dari robot ini digunakan untuk menekan tombol power komputer.

## Behind the Scene
Ada dua bagian utama dalam sistem ini,
1. Web control panel [(main.go)](main.go)
2. Servo aktuator [(servo.py)](servo.py)

Kedua aplikasi terebut dikontrol oleh satu konfigurasi yang berasal dari file yang sama,
yakni [hosts.ini](hosts.ini). Format konfigurasi sederhana dengan menggunakan conf/ini file.

Berikut contoh `hosts.ini`
```conf
[default]
version=0.1.0

[main-server]
gpio=11 # perlu untuk servo aktuator
hostname=alarm.local # avahi mdns
name=Ilkomunity
desc=Server Komunitas u/ Himalkom

[backup]
gpio=12 # gpio pin 18
hostname=10.60.1.43 # ip atau local discoverable domain menggunakan avahi/mdns
name=Ilkomunity Backup
desc=Server Komunitas u/ Himalkom - Backup
```

Karena design awal tidak ada yang berkaitan langsung dengan power dari komputer, mendapatkan
state hidup atau mati dari komputer server ini menjadi lebih susah. Oleh karena itu, sistem ini
mengasumsikan bahwa yang terjadi adalah tidak ada gannguan dalam jaringan internet lokal.

Aktuator Servo (servo.py) ini bekerja berdasarkan webhook yang terpasang pada `/host/:machine/fire`,
dimana jika user mengunjungi alamat URL tersebut maka pada web server akan menjalakan aktuator servo
di latarbelakang.

## Screenshots dan Video

