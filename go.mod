module pstr

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	gopkg.in/ini.v1 v1.57.0
)
