package main

import (
	"log"
	"net/http"
	"os/exec"

	"github.com/gin-gonic/gin"
	"gopkg.in/ini.v1"
)

// var cfgFilename string
var cfg *ini.File
var machines []string

func init() {
	var err error

	cfg, err = ini.Load("hosts.ini")
	if err != nil {
		log.Panicln(err)
	}

	version := cfg.Section(ini.DEFAULT_SECTION).Key("version").String()
	if version != "0.1.0" { // hardcoded value FTW
		log.Panicln("incorrect hosts.ini version")
	}

	machines = cfg.SectionStrings()[1:]
	log.Printf("[PSTR] configuration loaded, version %s, with %d configured hosts.",
		version,
		len(machines),
	)
}

func hostnameHandler(c *gin.Context) {
	machine := c.Param("machine")

	section, err := cfg.GetSection(machine)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	hostname := section.Key("hostname").String()
	name := section.Key("name").String()
	desc := section.Key("desc").String()

	if hostname == "" {
		// shouldn't have any empty hostname here
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.HTML(http.StatusOK, "host.tmpl.html", gin.H{
		"machine":  machine,
		"machines": machines,
		"hostname": hostname,
		"name":     name,
		"desc":     desc,
	})
}

func hostnamePingHandler(c *gin.Context) {
	machine := c.Param("machine")

	section, err := cfg.GetSection(machine)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	hostname := section.Key("hostname").String()
	if hostname == "" {
		// shouldn't have any empty hostname here
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	out, err := exec.Command("ping", "-c", "4", hostname).Output()
	if err != nil {
		log.Fatal(err)
		c.String(http.StatusInternalServerError, "failed")
		return
	}

	c.String(http.StatusOK, "%s", out)
}

func fire(machine string) {
	cmd := exec.Command("python", "/home/alarm/toggle.py")
	_, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
}

func hostnameToggleHandler(c *gin.Context) {
	machine := c.Param("machine")

	go fire(machine)

	c.String(http.StatusOK, "Servo fired! Check status to check if machine is already up.")
}

func main() {
	router := gin.Default()
	router.LoadHTMLGlob("templates/*")

	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	router.GET("", func(c *gin.Context) {
		c.HTML(http.StatusOK, "main.tmpl.html", gin.H{
			"machines": machines,
		})
	})

	host := router.Group("/host")
	host.GET("/:machine", hostnameHandler)
	host.GET("/:machine/ping", hostnamePingHandler)
	host.GET("/:machine/fire", hostnameToggleHandler)

	router.Run(":8080")
}
