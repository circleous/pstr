import configparser
import os
import sys
import time

import RPi.GPIO as GPIO

config = configparser.ConfigParser()
config.read('hosts.ini')


def main():
  if len(sys.argv) < 2:
    print("Usage: %s <machine>" % sys.argv[0])
    exit()

  machine = sys.argv[1]

  if machine not in config:
    print("not found")
    exit()

  gpioPin = int(config[machine]['gpio'])

  GPIO.setwarnings(False)
  GPIO.setmode(GPIO.BOARD)
  GPIO.setup(gpioPin, GPIO.OUT)

  Servo = GPIO.PWM(gpioPin, 50)
  Servo.start(12.5)
  time.sleep(2)

  steps = 3
  stepslength = 12.5 / int(steps)
  for Counter in range(int(steps)):
    Servo.ChangeDutyCycle(12.5 - (stepslength * (Counter + 1)))
    time.sleep(0.5)

  time.sleep(1)
  Servo.stop()
  GPIO.cleanup()


if __name__ == "__main__":
  main()
